fn main() {
    let my_pid:u32 =  std::process::id();
    println!("Hello, my process id is {}", my_pid);
    if my_pid == 1 {
        println!("I'm init !");
    }
    else {
        println!("I'm not init :(");
    }
}
