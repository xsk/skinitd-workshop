use std::{process, env};

fn main() {
    let my_pid: u32 = process::id();

    println!("Hello, my process id is {}", my_pid);

    if my_pid == 1 {
        println!("I'm init !");
    }
    else {
        println!("I'm not init :(");
        // TODO : Specify how much of a problem this is
        //process::exit(0x1);
    }

    // Get the argument(s). Expecting the first process.
    let mut args: Vec<String> = env::args().collect();

    // TODO : Check if user provided no arguments.
    // Remove ourselves from the argument list.
    let process_names: Vec<String> =  args.drain(0..2).collect();

    // Get the process name.
    let proccess_name: String = process_names[1].to_string();
    
    println!("I will try to start `{}` with arguments {:?}", proccess_name, args);

    let process_output = process::Command::new(proccess_name)
        .args(&args)
        .spawn()
        .expect("Command failed to start");

    println!("{:?}", process_output.wait_with_output());
    
}
