#!/bin/bash

echo "Building image..."
docker build . -t skinitd:step2
echo 
echo "Running, expecting our init process to start nginx."
docker run --rm skinitd:step2
